#ifndef RESVIEWSPRITE_H
#define RESVIEWSPRITE_H

#include <QWidget>
#include <QPaintEvent>
#include "projectmodel.h"
#include "itimelineresponder.h"
#include "imodelistresponder.h"
#include <QTimer>

/**
  * Class for viewing a sprite resource.
  * Responds to timeline events.
  * Responds to draw tools.
  * Responds to mend tools.
  */

class resViewSprite : public QWidget, public ITimelineResponder, public IModeListResponder
{
    Q_OBJECT
public:
    explicit resViewSprite(QWidget *parent = 0);
    explicit resViewSprite(Sprite* spr, QWidget *parent = 0);
    void paintEvent(QPaintEvent *event);
    SpriteFrame currentFrame();
    SpriteMode currentMode();
    SpriteFrame& refCurrentFrame();
    SpriteMode& refCurrentMode();

    // == ITimelineResponder interface
    void play();
    void pause();
    void frameUp();
    void frameDown();
    void frameFirst();
    void frameLast();
    void setFrame(int);
    int  getFrameCount();
    int  getFrame();
    // ==

    // == IModeListResponder interface
    QString getMode(int index);
    int     getModeCount();
    void    setMode(int index);
    void    addMode(QString name);
    void    removeMode(int index);
    void    renameMode(int index, QString name);
    // ==

    Sprite* mSprite;
    QString curMode;
    int frame;
    bool playing;
    int playSpeed;
    QPoint displacement;
    float zoom;

signals:

public slots:
    void nextFrame();

};

#endif // RESVIEWSPRITE_H
