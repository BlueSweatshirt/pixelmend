#ifndef PROJECTMODEL_H
#define PROJECTMODEL_H

#include <Qt/qstring.h>
#include <Qt/qlist.h>
#include <Qt/qimage.h>
#include <Qt/qpixmap.h>
#include <Qt/qpair.h>
#include <QAbstractListModel>
#include <QItemSelection>
#include "imodelistresponder.h"

class SpriteFrame
{
public:
    QPoint anchor;
    QVector<QPoint> pivots;
    QPixmap image;
    float angle;
    static const QPoint invalidJoint; // to mark a joint as invalid within the program even if the memory for that point still exists.
};

class SpriteMode
{
public:
    QString name;
    QVector<SpriteFrame> frames;
};

class Sprite
{
public:
    QString name;
    QString defaultMode;
    QList< SpriteMode > modes;
};

class CompositeMode
{
public:
    QString name;
    Sprite* linkSprite;
    int linkPivot;
    Sprite* mySprite;
};

class Composite
{
public:
    void buildModeList();

    QString name;
    QString defaultMode;
    QList< QString > modeList;
    QList< CompositeMode > modes;
};

class Project
{
public:
    void load();
    void save();

    QPixmap loadSheet(QString filename);

    int mVersion;
    QString mFilename;

    QList< QPair<QString, Sprite> > mSprites;
    QList< QPair<QString, Composite> > mComposites;
    QList< QPair<QString, QPixmap> > mSheets;
};

// Class used to populate a ListView with a resource list
class ResModel : public QAbstractListModel
{
    Q_OBJECT
public:

    QVariant data( const QModelIndex & index, int role = Qt::DisplayRole ) const;
    int rowCount(const QModelIndex& index) const;
    void addSprite(Sprite nSprite);
    void addImage(QString fname);
    void removeResource(int index);

    Project* mProject;
};

// Class used to populate a ListView with a sprite mode list, taking advantage of IModeListResponder
class ModeModel : public QAbstractListModel
{
    Q_OBJECT
public:

    QVariant data( const QModelIndex & index, int role = Qt::DisplayRole ) const;
    int rowCount(const QModelIndex& index) const;
    void selectionChanged ( const QItemSelection & selected, const QItemSelection & deselected );
    void addMode(QString name);
    void removeMode(int index);
    void renameMode(int index, QString name);

    IModeListResponder* mModes;
    int selection;
};

#endif // PROJECTMODEL_H
