#include "modenamedialog.h"
#include "ui_modenamedialog.h"

ModeNameDialog::ModeNameDialog(QString name) :
    QDialog(0),
    ui(new Ui::ModeNameDialog)
{
    ui->setupUi(this);
    ui->lineEdit->setText(name);
    ui->lineEdit->setFocus();
    ui->lineEdit->selectAll();
}

ModeNameDialog::~ModeNameDialog()
{
    delete ui;
}

QString ModeNameDialog::getResultName()
{
    return ui->lineEdit->text();
}
