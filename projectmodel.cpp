#include "projectmodel.h"
#include <Qt/qdebug.h>
#include <QItemSelection>

const QPoint SpriteFrame::invalidJoint = QPoint(-256, -256);

void Composite::buildModeList()
{
    modeList.clear();
    for(QList<CompositeMode>::iterator it = modes.begin(); it != modes.end(); ++it)
    {
        for(QList<SpriteMode>::iterator iz = it->mySprite->modes.begin(); iz != it->mySprite->modes.end(); ++it)
        {
            QString candidate = iz->name;
            if (!modeList.contains(candidate))
                modeList.append(candidate);
        }
    }
}

/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////

QPixmap Project::loadSheet(QString filename)
{
    QPair<QString, QPixmap> sheet;
    QPixmap map;
    if (!map.load(filename))
    {
        // Yell at someone
        qDebug() << "File not found: " << filename; // for now we'll use qDebug's output stream to debug. In the future we can do GUI messages or something I guess.
        return QPixmap();
    }
    sheet.first = filename.right(filename.length()-filename.lastIndexOf("/")-1);
    sheet.second = map;
    mSheets.append(sheet);
    return sheet.second;
}

/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////

int ResModel::rowCount(const QModelIndex& index) const
{
    if (!mProject) return 0;
    return mProject->mSheets.count() + mProject->mSprites.count() + mProject->mComposites.count();
}

QVariant ResModel::data( const QModelIndex & index, int role) const
{
    // This is structured in a way that images are always showed before sprites which are always shown before composites.
    // This allows us to easily discern which resource type to handle while also creating some resource organization at the same time.y
    if (!mProject) return QVariant();

    if (role == Qt::DisplayRole)
    {
        if (index.row() < mProject->mSheets.count())
            return "I - " + QString(mProject->mSheets.at(index.row()).first);
        else if (index.row() < mProject->mSheets.count() + mProject->mSprites.count())
            return "S - " + QString(mProject->mSprites.at(index.row()-mProject->mSheets.count()).first);
        else if (index.row() < mProject->mSheets.count() + mProject->mSprites.count() + mProject->mComposites.count())
            return "C - " + QString(mProject->mComposites.at(index.row()-mProject->mSheets.count()-mProject->mSprites.count()).first);
        else
            return QVariant();
    }
    return QVariant();
}

void ResModel::addSprite(Sprite nSprite)
{
    beginInsertRows(QModelIndex(), mProject->mSheets.count() + mProject->mSprites.count(), mProject->mSheets.count() + mProject->mSprites.count());
    mProject->mSprites.append(QPair<QString, Sprite>(nSprite.name, nSprite));
    endInsertRows();
}

void ResModel::addImage(QString fname)
{
    beginInsertRows(QModelIndex(), mProject->mSheets.count(), mProject->mSheets.count());
    mProject->loadSheet(fname);
    endInsertRows();
}

void ResModel::removeResource(int index)
{
    beginRemoveRows(QModelIndex(), index, index);
    if (index < mProject->mSheets.length())
    {
        mProject->mSheets.removeAt(index);
    }
    else if (index < mProject->mSheets.length() + mProject->mSprites.length())
    {
        mProject->mSprites.removeAt(index-mProject->mSheets.length());
    }
    else if (index < mProject->mSheets.length() + mProject->mSprites.length() + mProject->mComposites.length())
    {
        mProject->mComposites.removeAt(index-mProject->mSheets.length()-mProject->mComposites.length());
    }
    endRemoveRows();
}

/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////

int ModeModel::rowCount(const QModelIndex& index) const
{
    if (!mModes) return 0;
    return mModes->getModeCount();
}

QVariant ModeModel::data( const QModelIndex & index, int role) const
{
    if (!mModes) return QVariant();

    if (role == Qt::DisplayRole)
    {
        return mModes->getMode(index.row());
    }
    return QVariant();
}

void ModeModel::addMode(QString name)
{
    if (!mModes) return;
    beginInsertRows(QModelIndex(), mModes->getModeCount(), mModes->getModeCount());
    mModes->addMode(name);
    endInsertRows();
}

void ModeModel::removeMode(int index)
{
    if (!mModes) return;
    beginRemoveRows(QModelIndex(), index, index);
    mModes->removeMode(index);
    endRemoveRows();
}

void ModeModel::renameMode(int index, QString name)
{
    if (!mModes) return;
    beginResetModel();
    mModes->renameMode(index, name);
    endResetModel();
}
