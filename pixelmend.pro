#-------------------------------------------------
#
# Project created by QtCreator 2012-09-07T22:44:22
#
#-------------------------------------------------

QT       += core gui

TARGET = pixelmend
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    projectmodel.cpp \
    resviewimage.cpp \
    resviewsprite.cpp \
    itimelineresponder.cpp \
    imodelistresponder.cpp \
    modenamedialog.cpp \
    jointeditdialog.cpp \
    anchoreditdialog.cpp \
    setimagedialog.cpp

HEADERS  += mainwindow.h \
    projectmodel.h \
    resviewimage.h \
    resviewsprite.h \
    itimelineresponder.h \
    imodelistresponder.h \
    modenamedialog.h \
    jointeditdialog.h \
    anchoreditdialog.h \
    setimagedialog.h

FORMS    += mainwindow.ui \
    modenamedialog.ui \
    jointeditdialog.ui \
    anchoreditdialog.ui \
    setimagedialog.ui
