#ifndef JOINTEDITDIALOG_H
#define JOINTEDITDIALOG_H

#include <QDialog>
#include "projectmodel.h"

namespace Ui {
class JointEditDialog;
}

class JointEditDialog : public QDialog
{
    Q_OBJECT
    
public:
    explicit JointEditDialog(SpriteFrame res);
    ~JointEditDialog();

    void setJoint(int index);
    SpriteFrame getChangedFrame();
    
private slots:
    void on_editJoint_valueChanged(int arg1);

    void on_editX_valueChanged(int arg1);

    void on_editY_valueChanged(int arg1);

    void on_removeButton_clicked();

private:
    Ui::JointEditDialog *ui;
    SpriteFrame mFrame;
    int jointIndex;
};

#endif // JOINTEDITDIALOG_H
