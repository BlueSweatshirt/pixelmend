#include "setimagedialog.h"
#include "ui_setimagedialog.h"

SetImageDialog::SetImageDialog(Project* proj) :
    QDialog(0),
    ui(new Ui::SetImageDialog)
{
    ui->setupUi(this);
    mProject = proj;

    for(QList<QPair<QString, QPixmap> >::iterator i = mProject->mSheets.begin(); i != mProject->mSheets.end(); ++i)
    {
        ui->editImage->addItem(i->first); // store the integer index as user data for easy access
    }
}

SetImageDialog::~SetImageDialog()
{
    delete ui;
}

QPixmap SetImageDialog::getNewImage()
{
    QPixmap src;
    for(QList<QPair<QString, QPixmap> >::iterator i = mProject->mSheets.begin(); i != mProject->mSheets.end(); ++i)
    {
        if (i->first != ui->editImage->itemText(ui->editImage->currentIndex())) continue;
        src = i->second;
    }
    //QPixmap src = mProject->mSheets.at(ui->editImage->itemData(ui->editImage->currentIndex()).toInt()).second;
    return src.copy(ui->editX->value(), ui->editY->value(), ui->editW->value(), ui->editH->value());
}
