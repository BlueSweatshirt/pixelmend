#include "resviewimage.h"
#include <QPainter>

resViewImage::resViewImage(QWidget *parent) :
    QWidget(parent)
{
    //
}


resViewImage::resViewImage(QPixmap map, QWidget *parent) :
    QWidget(parent)
{
    mImage = map;
}

void resViewImage::paintEvent(QPaintEvent *event)
{
    QPainter brush(this);
    int dX = (this->size().width() / 2) - (mImage.size().width() / 2);
    int dY = (this->size().height() / 2) - (mImage.size().height() / 2);
    brush.drawPixmap(dX, dY, mImage);
    brush.end();
}
