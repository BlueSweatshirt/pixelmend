#ifndef IMODELISTRESPONDER_H
#define IMODELISTRESPONDER_H

#include <QString>

/**
  * Abstract interface for providing(and receiving) information about sprite modes.
  * This class is utilized by ModeModel(projectmodel.h/cpp) and managed by MainWindow
  * Call the setModer() method in MainWindow to attach a child resource instance of IModeListResponder to the ui views.
  */

class IModeListResponder
{
public:
    virtual QString getMode(int index)    = 0; // Get mode at provided index
    virtual int     getModeCount()        = 0; // Get number of modes
    virtual void    setMode(int index)    = 0; // Set the mode at the given index.
    virtual void    addMode(QString name) = 0; // Add a new mode with the given name.
    virtual void    removeMode(int index) = 0; // Remove mode at index.
    virtual void    renameMode(int index, QString name) = 0; // Rename mode at index
};

#endif // IMODELISTRESPONDER_H
