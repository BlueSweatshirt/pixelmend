#include "anchoreditdialog.h"
#include "ui_anchoreditdialog.h"

AnchorEditDialog::AnchorEditDialog(QPoint point) :
    QDialog(0),
    ui(new Ui::AnchorEditDialog)
{
    ui->setupUi(this);
    pt = point;
    ui->editX->setValue(pt.x());
    ui->editY->setValue(pt.y());
}

AnchorEditDialog::~AnchorEditDialog()
{
    delete ui;
}

void AnchorEditDialog::on_editX_valueChanged(int arg1)
{
    pt.setX(arg1);
}

void AnchorEditDialog::on_editY_valueChanged(int arg1)
{
    pt.setY(arg1);
}

QPoint AnchorEditDialog::getChangedPoint()
{
    return pt;
}
