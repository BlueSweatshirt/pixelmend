#ifndef SETIMAGEDIALOG_H
#define SETIMAGEDIALOG_H

#include <QDialog>
#include "projectmodel.h"

namespace Ui {
class SetImageDialog;
}

class SetImageDialog : public QDialog
{
    Q_OBJECT
    
public:
    explicit SetImageDialog(Project* proj);
    ~SetImageDialog();

    QPixmap getNewImage();
    
private:
    Ui::SetImageDialog *ui;
    Project* mProject;
};

#endif // SETIMAGEDIALOG_H
