#ifndef ITIMELINERESPONDER_H
#define ITIMELINERESPONDER_H

/**
  * Abstract interface for responding to the timeline UI controls.
  * MainWindow has a method called setTimeliner() which should be used to assign a child class instance to respond to timeline events triggered by the UI.
  */

class ITimelineResponder
{
public:
    virtual void play() = 0; // Play the animation
    virtual void pause() = 0; // Pause the animation
    virtual void frameUp() = 0; // Go back a frame
    virtual void frameDown() = 0; // Go forward a frame
    virtual void frameFirst() = 0; // Go to the first frame
    virtual void frameLast() = 0; // Go to the last frame
    virtual void setFrame(int) = 0; // Set the frame to a certain index
    virtual int  getFrameCount() = 0; // Get the number of frames in the animation
    virtual int  getFrame() = 0; // Get the current frame of the animation
};

#endif // ITIMELINERESPONDER_H
