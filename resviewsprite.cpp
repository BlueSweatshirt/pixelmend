#include "resviewsprite.h"
#include <QPainter>
#include <QDebug>

resViewSprite::resViewSprite(QWidget *parent) :
    QWidget(parent)
{
    //
}


resViewSprite::resViewSprite(Sprite* spr, QWidget *parent) :
    QWidget(parent)
{
    mSprite = spr;
    curMode = mSprite->defaultMode;
    frame = 0;
    playing = false;
    playSpeed = 500;
    displacement.setX(0);
    displacement.setY(0);
    zoom = 1.0f;
}

void resViewSprite::paintEvent(QPaintEvent *event)
{
    if (currentMode().frames.empty()) return;
    QPainter brush(this);
    SpriteFrame curFrame = currentFrame();
    int dX = (this->size().width() / 2) - displacement.x() - (curFrame.image.width() / 2);
    int dY = (this->size().height() / 2) - displacement.y() - (curFrame.image.height() / 2);
    brush.drawPixmap(dX, dY, curFrame.image);
    QPen thePen;
    // Draw joints
    for (QVector<QPoint>::iterator i = curFrame.pivots.begin(); i != curFrame.pivots.end(); ++i)
    {
        if (*i == SpriteFrame::invalidJoint) continue;
        thePen.setColor(QColor(0, 255, 255));
        brush.setPen(thePen);
        brush.drawRect(dX + i->x() - 1, dY + i->y() - 1, 3, 3);
        thePen.setColor(QColor(255, 255 ,255));
        brush.setPen(thePen);
        brush.drawPoint(dX + i->x(), dY + i->y());
    }
    // Draw origin point
    thePen.setColor(QColor(255, 0, 0));
    brush.setPen(thePen);
    brush.drawRect(dX + curFrame.anchor.x() - 1, dY + curFrame.anchor.y() - 1, 3, 3);
    thePen.setColor(QColor(255, 255, 255));
    brush.setPen(thePen);
    brush.drawPoint(dX + curFrame.anchor.x(), dY + curFrame.anchor.y());
    //
    brush.end();
}

SpriteFrame resViewSprite::currentFrame()
{
    return currentMode().frames.at(frame);
}

SpriteMode resViewSprite::currentMode()
{
    for(QList<SpriteMode>::iterator it = mSprite->modes.begin(); it != mSprite->modes.end(); ++it)
    {
        if (it->name == curMode) return *it;
    }
    return SpriteMode();
}

void resViewSprite::nextFrame()
{
    if (!playing) return;
    frame++;
    if (frame >= currentMode().frames.size()) frame = 0;
    QTimer::singleShot(playSpeed, this, SLOT(nextFrame()));
    repaint(geometry());
}

SpriteFrame& resViewSprite::refCurrentFrame()
{
    return refCurrentMode().frames[frame];
}

SpriteMode& resViewSprite::refCurrentMode()
{
    for(QList<SpriteMode>::iterator it = mSprite->modes.begin(); it != mSprite->modes.end(); ++it)
    {
        if (it->name == curMode) return *it;
    }
    return SpriteMode();
}

// == ITimelineResponder interface

void resViewSprite::play()
{
    playing = true;
    QTimer::singleShot(playSpeed, this, SLOT(nextFrame()));
}

void resViewSprite::pause()
{
    playing = false;
}

void resViewSprite::frameDown()
{
    frame++;
    if (frame >= currentMode().frames.size()) frame--;
    playing = false;
    repaint(geometry());
}

void resViewSprite::frameUp()
{
    frame--;
    if (frame < 0) frame = 0;
    playing = false;
    repaint(geometry());
}

void resViewSprite::frameFirst()
{
    frame = 0;
    playing = false;
    repaint(geometry());
}

void resViewSprite::frameLast()
{
    frame = currentMode().frames.size() - 1;
    playing = false;
    repaint(geometry());
}

void resViewSprite::setFrame(int i)
{
    frame = i;
    playing = false;
    repaint(geometry());
}

int resViewSprite::getFrameCount()
{
    return currentMode().frames.size();
}

int resViewSprite::getFrame()
{
    return frame;
}

// == IModeListResponder interface

QString resViewSprite::getMode(int index)
{
    return mSprite->modes.at(index).name;
}

int resViewSprite::getModeCount()
{
    return mSprite->modes.size();
}

void resViewSprite::setMode(int index)
{
    curMode = mSprite->modes.at(index).name;
    playing = false;
    frame = 0;
    repaint(geometry());
}

void resViewSprite::addMode(QString name)
{
    SpriteMode mode;
    mode.name = name;
    mSprite->modes.append(mode);
    //curMode = name;
    playing = false;
    frame = 0;
    repaint(geometry());
}

void resViewSprite::removeMode(int index)
{
    mSprite->modes.removeAt(index);
    curMode = mSprite->modes.last().name;
    playing = false;
    frame = 0;
    repaint(geometry());
}

void resViewSprite::renameMode(int index, QString name)
{
    mSprite->modes[index].name = name;
}
