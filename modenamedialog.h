#ifndef MODENAMEDIALOG_H
#define MODENAMEDIALOG_H

#include <QDialog>

namespace Ui {
class ModeNameDialog;
}

class ModeNameDialog : public QDialog
{
    Q_OBJECT
    
public:
    explicit ModeNameDialog(QString name = "");
    ~ModeNameDialog();

    QString getResultName();
    
private:
    Ui::ModeNameDialog *ui;
};

#endif // MODENAMEDIALOG_H
