#include "jointeditdialog.h"
#include "ui_jointeditdialog.h"

JointEditDialog::JointEditDialog(SpriteFrame res) :
    QDialog(0),
    ui(new Ui::JointEditDialog)
{
    ui->setupUi(this);
    mFrame = res;
    setJoint(0);
}

JointEditDialog::~JointEditDialog()
{
    delete ui;
}

void JointEditDialog::setJoint(int index)
{
    jointIndex = index;
    ui->editJoint->setValue(index);
    int x = mFrame.pivots.value(index, QPoint(0, 0)).x();
    int y = mFrame.pivots.value(index, QPoint(0, 0)).y();
    ui->editX->setValue(x);
    ui->editY->setValue(y);
    if (mFrame.pivots.size() < jointIndex+1) mFrame.pivots.resize(jointIndex+1);
}

SpriteFrame JointEditDialog::getChangedFrame()
{
    return mFrame;
}

void JointEditDialog::on_editJoint_valueChanged(int arg1)
{
    setJoint(arg1);
}

void JointEditDialog::on_editX_valueChanged(int arg1)
{
    mFrame.pivots.replace(jointIndex, QPoint(arg1, ui->editY->value()));
}

void JointEditDialog::on_editY_valueChanged(int arg1)
{
    mFrame.pivots.replace(jointIndex, QPoint(ui->editX->value(), arg1));
}

void JointEditDialog::on_removeButton_clicked()
{
    mFrame.pivots[jointIndex] = SpriteFrame::invalidJoint;
    setJoint(jointIndex); // to refresh the spinner boxes
}
