#ifndef ANCHOREDITDIALOG_H
#define ANCHOREDITDIALOG_H

#include <QDialog>

namespace Ui {
class AnchorEditDialog;
}

class AnchorEditDialog : public QDialog
{
    Q_OBJECT
    
public:
    explicit AnchorEditDialog(QPoint point = QPoint());
    ~AnchorEditDialog();

    QPoint getChangedPoint();
    
private slots:
    void on_editX_valueChanged(int arg1);

    void on_editY_valueChanged(int arg1);

private:
    Ui::AnchorEditDialog *ui;
    QPoint pt;
};

#endif // ANCHOREDITDIALOG_H
