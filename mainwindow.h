#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "projectmodel.h"
#include "itimelineresponder.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void setTimeliner(ITimelineResponder* t); // hopefully this is self explanatory haha
    void setModer(IModeListResponder* m);

    QAction* actionNewSprite;
    QAction* actionNewImage;
    QAction* actionNewComposite;
    QAction* actionRemoveResource;
    
private slots:

    void newSprite();
    void newImage();
    void newComposite();
    void removeResource();

    void on_resView_tabCloseRequested(int index);
    void on_resList_doubleClicked(const QModelIndex &index);
    void on_resView_currentChanged(QWidget *arg1);
    void on_playButton_clicked();
    void on_frameBackButton_clicked();
    void on_frameForwardButton_clicked();
    void on_frameFirst_clicked();
    void on_frameLast_clicked();
    void on_frameSlider_sliderMoved(int position);
    void on_modeList_clicked(const QModelIndex &index);
    void on_modeAdd_clicked();
    void on_modeRemove_clicked();
    void on_modeRename_clicked();
    void on_editJoints_clicked();
    void on_editAnchor_clicked();
    void on_editFrameImage_clicked();

    void on_resList_customContextMenuRequested(const QPoint &pos);

    void on_addFrame_clicked();

    void on_removeFrame_clicked();

private:
    Ui::MainWindow *ui;
    Project* mProject;
    ITimelineResponder* mTimeliner;
    bool playing;
    ModeModel* modeModel;
    QWidget* curResTab;
};

#endif // MAINWINDOW_H
