#ifndef RESVIEWIMAGE_H
#define RESVIEWIMAGE_H

#include <QWidget>
#include <QPaintEvent>
#include "projectmodel.h"

/**
  * Class for viewing an image resource.
  * Does not respond to timeline events.
  * Responds to draw tools.
  * Does not respond to mend tools.
  */


class resViewImage : public QWidget
{
    Q_OBJECT
public:
    explicit resViewImage(QWidget *parent = 0);
    explicit resViewImage(QPixmap map, QWidget *parent = 0);

    void paintEvent(QPaintEvent *event);

    QPixmap mImage;
    
signals:
    
public slots:
    
};

#endif // RESVIEWIMAGE_H
