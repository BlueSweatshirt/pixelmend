#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "projectmodel.h"
#include "resviewimage.h"
#include "resviewsprite.h"
#include "modenamedialog.h"
#include "jointeditdialog.h"
#include "anchoreditdialog.h"
#include "setimagedialog.h"

#include <QStringListModel>
#include <QDebug>
#include <QDir>
#include <QCoreApplication>
#include <QLabel>
#include <QTabWidget>
#include <QMenu>
#include <QFileDialog>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    mTimeliner = 0;
    playing = false;

    mProject = new Project();

    // Test sheet loading
    QPixmap testSheet = mProject->loadSheet(QCoreApplication::applicationDirPath()+"/sheet.png");
    QPixmap testSheet2 = mProject->loadSheet(QCoreApplication::applicationDirPath()+"/sheet2.png");

    // Test sprite
    Sprite testSprite;
    testSprite.defaultMode = "Derp";
    SpriteMode testMode;
    SpriteFrame testFrame;
    //      frame 1
    testFrame.image = testSheet;
    testMode.frames.append(testFrame);
    //      frame 2
    testFrame.image = testSheet2;
    testFrame.anchor.setX(30);
    testMode.frames.append(testFrame);
    //      mode 1
    testMode.name = "Derp";
    testSprite.modes.append(testMode);
    //      mode 2
    testMode.name = "Lawl";
    testMode.frames.clear();
    testMode.frames.append(testFrame);
    testSprite.modes.append(testMode);
    //      Add to project
    mProject->mSprites.append(QPair<QString,Sprite>("test_sprite", testSprite));

    // Test composite
    mProject->mComposites.append(QPair<QString,Composite>("test_comp", Composite()));

    ResModel* resModel = new ResModel();
    resModel->mProject = mProject;
    ui->resList->setModel(resModel);

    modeModel = new ModeModel();
    modeModel->mModes = 0;
    ui->modeList->setModel(modeModel);

    // Create actions
    actionNewSprite = new QAction("New Sprite", this);
    connect(actionNewSprite, SIGNAL(triggered()), this, SLOT(newSprite()));
    actionNewImage = new QAction("New Image", this);
    connect(actionNewImage, SIGNAL(triggered()), this, SLOT(newImage()));
    actionNewComposite = new QAction("New Composite", this);
    connect(actionNewComposite, SIGNAL(triggered()), this, SLOT(newComposite()));
    actionRemoveResource = new QAction("Remove Resource", this);
    connect(actionRemoveResource, SIGNAL(triggered()), this, SLOT(removeResource()));

    // Connect menu actions
    connect(ui->actionNew_sprite, SIGNAL(triggered()), this, SLOT(newSprite()));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::setTimeliner(ITimelineResponder *t)
{
    mTimeliner = t;
    if (t == 0) return;
    ui->frameSlider->setMaximum(t->getFrameCount()-1);
    ui->frameSlider->setValue(t->getFrame());
    ui->frameLabel->setText(QString().setNum(t->getFrame()+1)+"/"+QString().setNum(t->getFrameCount()));
}

void MainWindow::setModer(IModeListResponder *m)
{
    ui->modeList->setModel(0);
    delete modeModel;
    modeModel = new ModeModel();
    modeModel->mModes = m;
    ui->modeList->setModel(modeModel);
    // TODO: don't reallocate memory--figure out the way to get the list to just refresh all its contents.
}

void MainWindow::on_resView_tabCloseRequested(int index)
{
    // TODO: Save final changes or smth?? if necessary
    ui->resView->removeTab(index);
}

void MainWindow::on_resList_doubleClicked(const QModelIndex &index)
{
    // Uses the same differentiation method as seen in ResModel(projectmodel.cpp/h)

    if (index.row() < mProject->mSheets.count())
    {
        ui->resView->addTab(new resViewImage(mProject->mSheets.at(index.row()).second), mProject->mSheets.at(index.row()).first);
    }
    else if (index.row() < mProject->mSheets.count() + mProject->mSprites.count())
    {
        ui->resView->addTab(new resViewSprite(const_cast<Sprite*>(&mProject->mSprites.at(index.row()-mProject->mSheets.count()).second)), mProject->mSprites.at(index.row()-mProject->mSheets.count()).first);
    }
    else if (index.row() < mProject->mSheets.count() + mProject->mSprites.count() + mProject->mComposites.count())
    {
        ui->resView->addTab(new QWidget(), mProject->mComposites.at(index.row()-mProject->mSheets.count()-mProject->mSprites.count()).first);
    }
}

void MainWindow::on_resView_currentChanged(QWidget *arg1)
{
    curResTab = arg1;

    setTimeliner(0);
    setModer(0);
    ui->editAnchor->setEnabled(false);
    ui->editConnections->setEnabled(false);
    ui->editJoints->setEnabled(false);
    ui->modeAdd->setEnabled(false);
    ui->modeRemove->setEnabled(false);
    ui->modeRename->setEnabled(false);
    ui->editFrameImage->setEnabled(false);

    resViewSprite* sprView = dynamic_cast<resViewSprite*>(arg1);
    if (sprView)
    {
        setTimeliner(sprView);
        setModer(sprView);
        ui->modeAdd->setEnabled(true);
        ui->modeRemove->setEnabled(true);
        ui->modeRename->setEnabled(true);
        ui->editAnchor->setEnabled(true);
        ui->editJoints->setEnabled(true);
        ui->editFrameImage->setEnabled(true);
    }
}

void MainWindow::on_playButton_clicked()
{
    if (!mTimeliner) return;
    playing = !playing;
    if (playing)
    {
        mTimeliner->play();
    }
    else
    {
        mTimeliner->pause();
    }
}

void MainWindow::on_frameBackButton_clicked()
{
    if (!mTimeliner) return;
    mTimeliner->frameUp();
}

void MainWindow::on_frameForwardButton_clicked()
{
    if (!mTimeliner) return;
    mTimeliner->frameDown();
}

void MainWindow::on_frameFirst_clicked()
{
    if (!mTimeliner) return;
    mTimeliner->frameFirst();
}

void MainWindow::on_frameLast_clicked()
{
    if (!mTimeliner) return;
    mTimeliner->frameLast();
}

void MainWindow::on_frameSlider_sliderMoved(int position)
{
    if (!mTimeliner) return;
    mTimeliner->setFrame(position);
    ui->frameLabel->setText(QString().setNum(mTimeliner->getFrame()+1)+"/"+QString().setNum(mTimeliner->getFrameCount()));
}

void MainWindow::on_modeList_clicked(const QModelIndex &index)
{
    if (!modeModel->mModes) return;
    modeModel->mModes->setMode(index.row());
}

void MainWindow::on_modeAdd_clicked()
{
    if (!modeModel->mModes) return;
    // TODO: make a dialogue window to get name for new sprite mode
    ModeNameDialog nameDialog("");
    if (nameDialog.exec() == QDialog::Accepted && nameDialog.getResultName() != "")
    {
        modeModel->addMode(nameDialog.getResultName()); // the model wraps the IModeListResponder interface in order to ensure the view updates
    }
}

void MainWindow::on_modeRemove_clicked()
{
    if (!modeModel->mModes) return;
    // TODO: "are you sure?" dialog? maybe?
    modeModel->removeMode(ui->modeList->selectionModel()->selection().first().indexes().first().row());
}

void MainWindow::on_modeRename_clicked()
{
    if (!modeModel->mModes) return;
    ModeNameDialog nameDialog(modeModel->mModes->getMode(ui->modeList->selectionModel()->selection().first().indexes().first().row()));
    if (nameDialog.exec() == QDialog::Accepted && nameDialog.getResultName() != "")
    {
        modeModel->renameMode(ui->modeList->selectionModel()->selection().first().indexes().first().row(), nameDialog.getResultName());
    }
}

void MainWindow::on_editJoints_clicked()
{
    resViewSprite* sprView = dynamic_cast<resViewSprite*>(curResTab);
    if (!sprView) return;
    JointEditDialog jointDialog(sprView->currentFrame());
    if (jointDialog.exec() == QDialog::Accepted)
    {
        sprView->refCurrentFrame() = jointDialog.getChangedFrame();
    }
}

void MainWindow::on_editAnchor_clicked()
{
    resViewSprite* sprView = dynamic_cast<resViewSprite*>(curResTab);
    if (!sprView) return;
    AnchorEditDialog anchorDialog(sprView->currentFrame().anchor);
    if (anchorDialog.exec() == QDialog::Accepted)
    {
        sprView->refCurrentFrame().anchor = anchorDialog.getChangedPoint();
    }
}

void MainWindow::newSprite()
{
    ModeNameDialog nameDialog("New sprite");
    if (nameDialog.exec() == QDialog::Accepted)
    {
        Sprite nSprite;
        nSprite.name = nameDialog.getResultName();
        SpriteMode nMode;
        nMode.name = "Default";
        nMode.frames.append(SpriteFrame());
        nSprite.modes.append(nMode);
        dynamic_cast<ResModel*>(ui->resList->model())->addSprite(nSprite); // wrapper so the view updates
    }
}

void MainWindow::on_editFrameImage_clicked()
{
    SetImageDialog imageDialog(mProject);
    if (imageDialog.exec() == QDialog::Accepted)
    {
        resViewSprite* sprView = dynamic_cast<resViewSprite*>(curResTab);
        sprView->refCurrentFrame().image = imageDialog.getNewImage();
    }
}

void MainWindow::newImage()
{
    QFileDialog fileDialog(this, "Select image to import", "/", "Image files (*.png *.jpg *.gif)");
    if (fileDialog.exec() == QDialog::Accepted)
    {
        QString fname = fileDialog.selectedFiles().first();
        dynamic_cast<ResModel*>(ui->resList->model())->addImage(fname); // wrapper so the view updates
    }
}

void MainWindow::newComposite()
{
    //
}

void MainWindow::removeResource()
{
    int index = ui->resList->selectionModel()->selection().first().indexes().first().row();
    dynamic_cast<ResModel*>(ui->resList->model())->removeResource(index); // wrapper so the view updates
}

void MainWindow::on_resList_customContextMenuRequested(const QPoint &pos)
{
    QMenu contextMenu(ui->resList);
    QList<QAction*> actList;
    actList.append(actionNewImage);
    actList.append(actionNewSprite);
    actList.append(actionNewComposite);
    actList.append(actionRemoveResource);
    contextMenu.addActions(actList);
    contextMenu.exec(ui->resList->mapToGlobal(pos));
}

void MainWindow::on_addFrame_clicked()
{
    resViewSprite* sprView = dynamic_cast<resViewSprite*>(curResTab);
    if (sprView)
    {
        sprView->refCurrentMode().frames.append(SpriteFrame());
        sprView->setFrame(sprView->refCurrentMode().frames.size() - 1);
        setTimeliner(sprView); // update the timeline UI
    }
}

void MainWindow::on_removeFrame_clicked()
{
    resViewSprite* sprView = dynamic_cast<resViewSprite*>(curResTab);
    if (sprView)
    {
        if (sprView->refCurrentMode().frames.size() == 1) return;
        sprView->refCurrentMode().frames.remove(sprView->frame);
        sprView->frame -= 1;
        if (sprView->frame < 0) sprView->frame = 0;
        sprView->repaint();
        setTimeliner(sprView); // update the timeline UI
    }
}
